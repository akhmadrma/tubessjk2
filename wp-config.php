<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'tubesbro' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'iA bgHOIPpuge|VESuc&Y$x^p:2I9^}=s8]1[-q6uj@-tFoG488%Of/+g,fM8u3@' );
define( 'SECURE_AUTH_KEY',  'tC`_Q`>1KUjtXq@6*8S3S5BU9l*{BTw0z*cx/lBk.e-QkwI&>G?N$qR4mBb@m*,/' );
define( 'LOGGED_IN_KEY',    '2dt:J#v]|c|%;D-^!+t+yc=H84cr xpNLN 7809=+d,.U (e `YqU>1GVUL9bi9i' );
define( 'NONCE_KEY',        '&vHy.+g]1ZbdWh;Dq*$!4oIx]hs_c,4d#;}h@rTlB?f9c~5{{f`>xc*xWrs+/r>v' );
define( 'AUTH_SALT',        'U2>h_t:j|gVaEedNzDRK0N>1ZiZ/xYS)Ch58+hH!8E>SPPUEi qJ6{(mO)f38I~j' );
define( 'SECURE_AUTH_SALT', 'I@LduWi]5|)yV*4x}o4Hz1EI;sgZL>H76qIcKA|`S2}HI.!9L#+=6XH/u4HKBaT^' );
define( 'LOGGED_IN_SALT',   '*qQ*MKRaIT(:H_Y./nC!Z[5^mXS`;S ]b9Z<G&Yt5hYEb=vx %+3$od?OW[dHOde' );
define( 'NONCE_SALT',       '. 9i&!w1}8{*]`}$<O<DC+aUOBeyyc;T W2w:@<moO}P<OUm`^~p<rNR&hJwT]Zp' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
